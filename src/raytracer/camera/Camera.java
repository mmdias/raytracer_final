package raytracer.camera;

import raytracer.cena.ViewPlane;
import raytracer.cena.World;
import raytracer.math.Vector3;

public interface Camera {
 
    void render(World world, ViewPlane vp);

    public Vector3 getEye();

    public Vector3 getLook();

    public Vector3 getUp();

}
