package raytracer.camera;

import raytracer.cena.ViewPlane;
import raytracer.cena.World;
import raytracer.math.Ray;
import raytracer.math.Vector2;
import raytracer.math.Vector3;
import raytracer.utility.UVW;
import static raytracer.cena.order.PixelArray.Pixel;

/**
 * Represents a pinhole perspective camera. The camera can focus all objects within the camera lens.
 */
public class PinholeCamera extends AbstractCamera {
    private float viewPlaneDistance;
    private float zoom;

    public PinholeCamera(Vector3 eyePosition, Vector3 lookPoint, Vector3 upDirection, float viewPlaneDistance) {
        super(eyePosition, lookPoint, upDirection);
        this.viewPlaneDistance = viewPlaneDistance;
        this.zoom = 1.0f;
    }

    public float getViewPlaneDistance() {
        return viewPlaneDistance;
    }

    public float getZoom() {
        return zoom;
    }

    public void setViewPlaneDistance(float viewPlaneDistance) {
        this.viewPlaneDistance = viewPlaneDistance;
    }

    public void setZoom(float zoom) {
        this.zoom = zoom;
    }

    @Override
    public void render(World w, ViewPlane vp) {
        UVW uvw = computeUVW();
        float s = vp.getS() / zoom;

        for (Pixel pixel : vp.getPixels()) {
            int c = pixel.getX();
            int r = pixel.getY();

            Vector3 L = new Vector3();

            for (int i = 0; i < vp.getSampler().getNumSamples(); i++) {
                Vector2 sp = vp.getSampler().nextSampleSquare();

                Vector2 pp = new Vector2(
                        s * (c - 0.5f * vp.getHRes() + sp.getX()),
                        s * (r - 0.5f * vp.getVRes() + sp.getY()));
                Ray ray = new Ray(eye, getDirection(pp, uvw));
                L.add(w.getTracer().trace(w, ray, 0));
            }

            L.divide(vp.getSampler().getNumSamples()).multiply(exposureTime);
            drawPixel(w, vp, c, r, L);
        }
    }

    public Vector3 getDirection(Vector2 p, UVW uvw) {
        return uvw.transform(p, -viewPlaneDistance);
    }
}
