package raytracer.camera;

import raytracer.cena.ViewPlane;
import raytracer.cena.World;
import raytracer.math.Vector3;
import raytracer.utility.UVW;
import static raytracer.math.Vector3.*;

public abstract class AbstractCamera implements Camera {
    protected Vector3 eye;
    protected Vector3 look;
    protected Vector3 up;

    protected float roll;
    protected float exposureTime;
   /* 
    private float viewPlaneDistance;

    private float focalDistance;
    private float lensRadius;
*/

    public AbstractCamera(Vector3 eyePosition, Vector3 lookPoint, Vector3 upDirection) {
        this.eye = eyePosition;
        this.look = lookPoint;
        this.up = upDirection;

        this.roll = 0;
        this.exposureTime = 1.0f;
    }

    @Override
	public Vector3 getEye() {
        return eye;
    }

    @Override
	public Vector3 getLook() {
        return look;
    }

    @Override
	public Vector3 getUp() {
        return up;
    }

    public float getRoll() {
        return roll;
    }

    public float getExposureTime() {
        return exposureTime;
    }

    public void setRoll(float roll) {
        this.roll = roll;
    }

    public void setExposureTime(float exposureTime) {
        this.exposureTime = exposureTime;
    }

    protected UVW computeUVW() {
        Vector3 w = subtract(eye, look).normalize();

        Vector3 up = rotate(this.up, w, roll);
        // take care of the singularity by hardwiring in specific camera orientations
        if (eye.getX() == look.getX() && eye.getZ() == look.getZ() && eye.getY() > look.getY()) { // camera looking vertically down
            return new UVW(
                    new Vector3(0, 0, 1),
                    new Vector3(1, 0, 0),
                    new Vector3(0, 1, 0));
        }

        if (eye.getX() == look.getX() && eye.getZ() == look.getZ() && eye.getY() < look.getY()) { // camera looking vertically up
            return new UVW(
                    new Vector3(1, 0, 0),
                    new Vector3(0, 0, 1),
                    new Vector3(0, -1, 0));
        }
        Vector3 u = cross(up, w).normalize();
        Vector3 v = cross(w, u);

        return new UVW(u, v, w);
    }


    public void drawPixel(World world, ViewPlane vp, int col, int row, Vector3 color) {
        if (vp.getGamma() != 1.0f)
            color.pow(vp.invGamma());

        int invR = vp.getVRes() - row - 1;
        world.drawPixel(col, invR, color);
    }
}
