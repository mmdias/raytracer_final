

package raytracer.brdf;


import raytracer.math.Vector3;
import raytracer.utility.ShadeRec;

import static raytracer.math.Vector3.multiply;


public class Difuse extends AbstractBRDF {
    private static final float INVPI = (float) (1.0 / Math.PI);

    private float kd;
    private Vector3 cd;

    /**
     * Creates a new Difuse with kd = 0 and black color.
     */
    public Difuse() {
        kd = 0;
        cd = new Vector3();
    }

    /**
     * @param kd Diffuse reflection coefficient.
     * @param cd Diffuse color
     */
    public Difuse(float kd, Vector3 cd) {
        this.kd = kd;
        this.cd = cd;
    }

    @Override
    public Vector3 f(ShadeRec sr, Vector3 wo, Vector3 wi) {
        return multiply(cd, kd * INVPI);
    }

    @Override
    public Vector3 rho(ShadeRec sr, Vector3 wo) {
        return multiply(cd, kd);
    }

    public float getKd() {
        return kd;
    }

    public Vector3 getCd() {
        return cd;
    }

    public void setKd(float kd) {
        this.kd = kd;
    }

    public void setCd(Vector3 cd) {
        this.cd = cd;
    }
}
