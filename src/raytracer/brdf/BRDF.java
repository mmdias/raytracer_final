package raytracer.brdf;

import raytracer.math.Vector3;
import raytracer.utility.FloatRef;
import raytracer.utility.ShadeRec;

/**
 * Interface for bidirectional reflectance distribution functions. These are functions that represent how the light is
 * reflected at surface.
 */
public interface BRDF {
    /**
     * Compute this BRDF.
     *
     * @param sr Surface point.
     * @param wo Reflected light direction.
     * @param wi Incoming light direction.
     * @return The luminance at point
     */
    Vector3 f(ShadeRec sr, Vector3 wo, Vector3 wi);

    /**
     * Samples the f function.
     *
     * @param sr Surface point.
     * @param wo Reflected light direction.
     * @param wi Incoming light direction.
     * @return The luminance at point
     */
    Vector3 sample_f(ShadeRec sr, Vector3 wo, Vector3 wi);

    /**
     * Samples the f function.
     *
     * @param sr  Surface point.
     * @param wo  Reflected light direction.
     * @param wi  Incoming light direction.
     * @param pdf Probability density function.
     * @return The luminance at point
     */
    Vector3 sample_f(ShadeRec sr, Vector3 wo, Vector3 wi, FloatRef pdf);

    /**
     * Returns the bihemispherical reflectance Phh
     *
     * @param sr Surface point.
     * @param wo Reflected light direction.
     * @return The the bihemispherical reflectance Phh
     */
    Vector3 rho(ShadeRec sr, Vector3 wo);
}
