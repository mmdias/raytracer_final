package raytracer.brdf;

import raytracer.math.Vector3;
import raytracer.utility.FloatRef;
import raytracer.utility.ShadeRec;

public abstract class AbstractBRDF implements BRDF {
    @Override
    public Vector3 sample_f(ShadeRec sr, Vector3 wo, Vector3 wi) {
        return new Vector3();
    }

    @Override
    public Vector3 sample_f(ShadeRec sr, Vector3 wo, Vector3 wi, FloatRef pdf) {
        return new Vector3();
    }

    @Override
    public Vector3 rho(ShadeRec sr, Vector3 wo) {
        return new Vector3();
    }
}
