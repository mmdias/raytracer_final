package raytracer.gui;

import raytracer.cena.ViewPlane;
import raytracer.cena.World;
import raytracer.cena.WorldListener;
import raytracer.cena.order.DrawOrder;
import raytracer.cena.order.DrawOrders;
import raytracer.math.Vector3;

import javax.imageio.ImageIO;
import javax.swing.*;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Locale;

public class SampleFrame extends JFrame {
	private static final int RaySpawns = 50;
	private static final long serialVersionUID = 1L;
    private static final String RENDER_INFO = " 100 raios Ray Tracer "  +
            " - Cena: %s - Tempo de Renderização: %s";
    private boolean renderToScreen = true;

    private JLabel output = new JLabel("");
    private JButton btnDraw = new JButton("Renderizar");
    private JButton btnSave = new JButton("Salvar");

    private JComboBox<WorldMaker> cmbScene = new JComboBox<>();
    private JComboBox<DrawOrder> cmbDrawOrder = new JComboBox<>();

    private JFileChooser chooser = new JFileChooser();
    private JProgressBar pbProgress = new JProgressBar();

    private WorldWaiter waiter;

    public SampleFrame() {
        super("Fundamentos de Computação Gráfica "  + "- Trabalho Final - Ray Tracer.");
        Locale.setDefault(Locale.US);

        waiter = new WorldWaiter();

        setResizable(false);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        output.setPreferredSize(new Dimension(800, 450));

        pbProgress.setString("");
        pbProgress.setStringPainted(true);

        JPanel pnlButtons = new JPanel(new FlowLayout());

        //cena
        pnlButtons.add(new JLabel("Cena:"));
        for (WorldMaker wm : WorldMaker.values()) {
            cmbScene.addItem(wm);
        }
        cmbScene.setSelectedItem(WorldMaker.TESTE);
        pnlButtons.add(cmbScene);

        //ordem dos raios
        pnlButtons.add(new JLabel("Ordem:"));
        for (DrawOrder drawOrder : DrawOrders.values()) {
            cmbDrawOrder.addItem(drawOrder);
        }
        cmbDrawOrder.setSelectedItem(DrawOrders.NORMAL);
        pnlButtons.add(cmbDrawOrder);

        //botoes
        btnDraw.addActionListener(e -> renderToScreen());
        pnlButtons.add(btnDraw);

        btnSave.addActionListener(e -> renderToFile());
        pnlButtons.add(btnSave);

        add(output, BorderLayout.CENTER);
        add(pnlButtons, BorderLayout.SOUTH);
        add(pbProgress, BorderLayout.NORTH);
        getRootPane().setDefaultButton(btnDraw);

        pack();
        setLocationRelativeTo(null);
        setVisible(true);
    }

    public static void main(String[] args) {
        new SampleFrame().setVisible(true);
    }

    private String formatRenderTime(long renderTime) {
        if (renderTime < 60000)
            return String.format("%.1f segundos", (renderTime / 1000.0));

        renderTime /= 1000;
        int s = (int) (renderTime % 60);
        renderTime /= 60;
        int m = (int) (renderTime % 60);

        String strMin = m == 1 ? "minuto" : "minutos";
        String strSec = s == 1 ? "segundo" : "segundos";
        return String.format("%d %s %d %s", m, strMin, s, strSec);
    }
    public void saveFile(World world, long renderTime) {
        File fileName = new File("Raytracer" .replace(".", "_") + 
        		"_" + world.getName() + ".png");
        chooser.setSelectedFile(fileName);

        if (chooser.showSaveDialog(this) != JFileChooser.APPROVE_OPTION)
            return;

        try {
            Graphics2D g2d = waiter.getImage().createGraphics();
            g2d.setColor(Color.BLACK);
            g2d.fillRect(0, 0, 1920, 25);
            g2d.setColor(Color.WHITE);
            g2d.setRenderingHint(RenderingHints
            		.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
            g2d.setFont(new Font("Arial", Font.BOLD, 16));
           
            g2d.dispose();
            ImageIO.write(waiter.getImage(), "png",
            		new FileOutputStream(chooser.getSelectedFile()));
        } catch (IOException e) {
            JOptionPane.showMessageDialog(this,
            		"Não foi possível salvar arquivo: " + chooser.getSelectedFile().getName());
        }
    }

    public void renderToScreen() {
        renderToScreen = true;

        ViewPlane vp = new ViewPlane(800, 450, RaySpawns);
        vp.setDrawOrder((DrawOrder) cmbDrawOrder.getSelectedItem());
        World world = ((WorldMaker) cmbScene.getSelectedItem()).
        		createScene(RaySpawns,1.0f, waiter);
        world.render(vp);
    }

    private void renderToFile() {
        renderToScreen = false;
        World world = ((WorldMaker) cmbScene.getSelectedItem())
        	.createScene(RaySpawns,2.4f, waiter);
        world.render(new ViewPlane(1920, 1080, RaySpawns));
    }

    public class WorldWaiter implements WorldListener {
        private int count;
        private BufferedImage image;
        private Graphics2D g2d;
        private long lastTimePainted;

        @Override
        public void traceStarted(World world, int width, int height) {
            image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);

            pbProgress.setMinimum(0);
            pbProgress.setMaximum(width * height);
            count = 0;
            EventQueue.invokeLater(() -> {
                        btnDraw.setEnabled(false);
                        btnSave.setEnabled(false);
                        if (renderToScreen) {
                            btnDraw.setText("Renderizando...");
                            output.setIcon(new ImageIcon(image));
                        } else {
                            btnSave.setText("Renderizando...");
                        }
                    }
            );


            g2d = image.createGraphics();
            g2d.setColor(new Color(world.getBackgroundColor().toRGB()));
            g2d.fillRect(0, 0, width, height);
            lastTimePainted = System.currentTimeMillis();
        }

        @Override
        public void pixelTraced(World world, int x, int y, Vector3 color) {
            image.setRGB(x, y, color.toRGB());
            count++;

            if (System.currentTimeMillis() - lastTimePainted > 500) {
                pbProgress.setValue(count);
                pbProgress.setString(String.format("Desenhando %s: %.2f%% - pixel %d of %d",
                        world.getName(), count * 100.0 / pbProgress.getMaximum(), count, pbProgress.getMaximum()));

                lastTimePainted = System.currentTimeMillis();
                repaint();
            }
        }

        @Override
        public void traceFinished(final World world, final long renderTime) {
        	         setTitle(String.format(RENDER_INFO, world.getName(), formatRenderTime(renderTime)));
        	             world.removeListener(this);
        	             EventQueue.invokeLater(() -> {
        	                         btnSave.setText("Salvar ");
        	                         btnSave.setEnabled(true);
        	                         btnDraw.setText("Renderizar");
        	                         btnDraw.setEnabled(true);
        	                         pbProgress.setValue(0);
        	                         pbProgress.setString("Finalizado!");
        	                         if (!renderToScreen) {
        	                             saveFile(world, renderTime);
        	                         }
        	                     }
        	             );
        	             repaint();
        	         }

        public BufferedImage getImage() {
            return image;
        }
    }
}