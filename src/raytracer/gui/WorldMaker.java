package raytracer.gui;

import raytracer.camera.ThinLensCamera;
import raytracer.cena.World;
import raytracer.cena.WorldListener;
import raytracer.light.AmbientOccludedLight;
import raytracer.light.AreaLight;
import raytracer.material.Emissivo;
import raytracer.material.Fosco;
import raytracer.material.Phong;
import raytracer.math.Vector3;
import raytracer.math.geometricas.compound.Compounds;
import raytracer.math.geometricas.primitivas.Argola;
import raytracer.math.geometricas.primitivas.CilindroA;
import raytracer.math.geometricas.primitivas.Esfera;
import raytracer.math.geometricas.primitivas.Plane;
import raytracer.math.geometricas.primitivas.Retangulo;
import raytracer.sampler.Sampler;
import raytracer.tracer.AreaLightTracer;
import raytracer.tracer.Raycasting;

public enum WorldMaker {

    
    BILLIARD {
        private static final float BALL_CM = 5.715f / 2.0f;

        private Esfera createCarom(float x, float z) {
            float caromCm = 6.15f / 2.0f;
            return new Esfera(new Vector3(x, caromCm, z), caromCm,
                    new Phong(0.2f, 0.65f, 0.4f, 64.00f, new Vector3(1, 1, 1)));
        }

        private Esfera createBall(float x, float z, Vector3 color) {
            Phong material = new Phong(0.2f, 0.65f, 0.4f, 64.00f, color);
            material.setCs(new Vector3(1, 1, 1));

            return new Esfera(new Vector3(x, BALL_CM, z), BALL_CM,
                    material);
        }

        private void createLamp(World world, float w, float y, float z, int numSamples) {
            float hw = w / 2.0f;

            Retangulo shape = new Retangulo(
                    new Vector3(-hw, y, -hw + z),
                    new Vector3(w, 0, 0),
                    new Vector3(0, 0, w),
                    new Vector3(0, -1, 0),
                    new Emissivo(40000, new Vector3(1, 1, 1)), Sampler.newDefault(numSamples));

            world.add(new AreaLight(shape));
            world.add(shape);
        }

        @Override
              public World createScene(int numSamples, float zoom, WorldListener listener) {

            ThinLensCamera camera = new ThinLensCamera(
                    new Vector3(-500, 100, 310),
                    new Vector3(0, 0, 0),
                    new Vector3(0, 1, 0),
                    2000, 700, 0.6f, Sampler.newDefault(numSamples));

            World world = new World(toString(), new AreaLightTracer(), new Vector3(0f, 0f, 0f), camera);
            world.setAmbientLight(new AmbientOccludedLight(1.5f, new Vector3(1.0f, 1.0f, 1.0f), 0.4f,
                    Sampler.newDefault(numSamples)));

            world.addListener(listener);

            //Lights
            createLamp(world, 20, 150, -40, numSamples);
            createLamp(world, 20, 150, -20, numSamples);
            createLamp(world, 20, 150, 0, numSamples);
            createLamp(world, 20, 150, 20, numSamples);
            createLamp(world, 20, 150, 40, numSamples);
 
            // colors
            Vector3 one = new Vector3(1.0f, 1.0f, 0.0f);            //Yellow
            Vector3 two = new Vector3(0.0f, 0.0f, 1.0f);            //Blue
            Vector3 three = new Vector3(1.0f, 0.0f, 0.0f);          //Red
            Vector3 four = new Vector3(0.29f, 0.0f, 0.3f);          //Purple
            Vector3 five = new Vector3(1.0f, 0.5f, 0.44f);           //Pink
            Vector3 six = new Vector3(0.0f, 0.41f, 0.41f);          //Green
            Vector3 seven = new Vector3(0.545f, 0.27f, 0.074f);     //Brown
            Vector3 eight = new Vector3(0.1f, 0.1f, 0.1f);            //Black

            Vector3 nine = new Vector3(1.0f, 1.0f, 0.0f);           //Yellow and White
            Vector3 ten = new Vector3(0.0f, 0.0f, 1.0f);            //Blue and white
            Vector3 eleven = new Vector3(1.0f, 0.0f, 0.0f);         //Red and white
            Vector3 twelve = new Vector3(0.29f, 0.0f, 0.3f);        //Purple and white
            Vector3 thirteen = new Vector3(1.0f, 0.5f, 0.44f);       //Pink and white
            Vector3 fourteen = new Vector3(0.0f, 0.41f, 0.41f);     //Green and white
            Vector3 fifteen = new Vector3(0.545f, 0.27f, 0.074f);   //Brown and white
            

            //Vector3 table = new Vector3(0.188f, 0.5f, 0.14f);       //Green
            Vector3 table = Vector3.fromRGB(102, 153, 153);

            // Table
            world.add(new Retangulo(new Vector3(-71, 0, -142), new Vector3(142, 0, 0), new Vector3(0, 0, 284), new Vector3(0, 1, 0),
                    new Fosco(0.2f, 0.5f, table)));

            // Balls
            world.add(createCarom(35, -122));
            world.add(createBall(50, 100, one)); //amarela

            world.add(createBall(-15, -100.0f, two)); //azul
            world.add(createBall(60, -92.0f, three)); //vermela

            world.add(createBall(-15, 98.0f, four)); //lilas
            world.add(createBall(-65, 120.0f, five)); //rosa
            world.add(createBall(45, -20.0f, six)); //verde

            world.add(createBall(25, 10.0f, seven)); //marrom
            world.add(createBall(30, 60.0f, eight)); //preta
            world.add(createBall(3, 100.0f, nine)); //amarelo branco
            world.add(createBall(-50, -110.0f, ten)); //azul branco

            world.add(createBall(-12, -130.0f, eleven)); // vermelho branco
            world.add(createBall(10, 130.0f, twelve)); // lilas branco
            world.add(createBall(0, -110.0f, thirteen)); //rosa branco
            world.add(createBall(-40, 10.0f, fourteen)); // verde branco
            world.add(createBall(-50, 15.0f, fifteen)); //marrom branco
          

            return world;
        }
    }, 
    TESTE{
        private void createLamp(World world, float w, float y, float z, int numSamples) {
            float hw = w / 2.0f;

            Retangulo shape = new Retangulo(
                    new Vector3(-hw, y, -hw + z),
                    new Vector3(w, 0, 0),
                    new Vector3(0, 0, w),
                    new Vector3(0, -1, 0),
                    new Emissivo(40000, new Vector3(1, 1, 1)), Sampler.newDefault(numSamples));

            world.add(new AreaLight(shape));
            world.add(shape);
            
        }
        
        public World createScene(int numSamples,float zoom, WorldListener listener) {
            ThinLensCamera camera = new ThinLensCamera(
                    new Vector3(-50, 80, 210),
                    new Vector3(0, 0, 0),
                    new Vector3(0, 1, 0),
                    400, 110, 0.25f, Sampler.newDefault(numSamples));

           // camera.setZoom(zoom);
            World world = new World(toString(), new Raycasting(), new Vector3(), camera);
            world.getBackgroundColor().set(0.7f, 0.7f, 0.7f);
            world.addListener(listener);

            //Lights
            world.setAmbientLight(new AmbientOccludedLight(1.0f, new Vector3(1.0f, 1.0f, 1.0f), 0.4f,
                    Sampler.newDefault(numSamples)));
            createLamp(world, 30, 100, -70, numSamples);
            createLamp(world, 30, 100, 70, numSamples);
            createLamp(world, 30, 100, 0, numSamples);
            
            Phong green = new Phong(0.2f, 0.65f, 0.4f, 64.0f, new Vector3(0.0f, 1.0f, 0.0f));
            Argola argola = new Argola(15, 3f, green);
            float angle1 = (float) Math.toRadians(90);
            float angle2 = (float) Math.toRadians(65);

            world.addInstance(argola).rotateX(angle1).translate(-40, 60, 0);

            world.addInstance(argola, green).rotateX(angle2).translate(20, 50, 0);
		
            world.add(new CilindroA(-10, 10, 60, new Fosco(0.2f, 0.65f, new Vector3(1.0f, 0.3f, 0.3f))));

            world.add(new Plane(new Vector3(0, -80, 0),
            		new Vector3(0, 1, 0), new Fosco
            		(0.1f, 0.9f, new Vector3(0, 1, 1))));
            return world;
            
        }
    	
    },
    OBJECTS_II {

        private void createLamp(World world, float w, float y, float z, int numSamples) {
            float hw = w / 2.0f;

            Retangulo shape = new Retangulo(
                    new Vector3(-hw, y, -hw + z),
                    new Vector3(w, 0, 0),
                    new Vector3(0, 0, w),
                    new Vector3(0, -1, 0),
                    new Emissivo(40000, new Vector3(1, 1, 1)), Sampler.newDefault(numSamples));

            world.add(new AreaLight(shape));
            world.add(shape);
        }


        @Override
        public World createScene(int numSamples,float zoom, WorldListener listener) {
            ThinLensCamera camera = new ThinLensCamera(
                    new Vector3(100, 180, 310),
                    new Vector3(0, 0, 0),
                    new Vector3(0, 1, 0),
                    400, 110, 0.25f, Sampler.newDefault(numSamples));

          // camera.setZoom(zoom);

            World world = new World(toString(), new Raycasting(), new Vector3(), camera);
            world.getBackgroundColor().set(0.7f, 0.7f, 0.7f);
            world.addListener(listener);

            //Lights
            world.setAmbientLight(new AmbientOccludedLight(1.0f, new Vector3(1.0f, 1.0f, 1.0f), 0.4f,
                    Sampler.newDefault(numSamples)));
            createLamp(world, 30, 100, -70, numSamples);
            createLamp(world, 30, 100, 70, numSamples);

            float angleX = (float) Math.toRadians(60);
            float angleY = (float) Math.toRadians(-45);
            float angleX1 = (float) Math.toRadians(-15);
            float angleY1 = (float) Math.toRadians(45);
            float angleX2 = (float) Math.toRadians(45);
            float angleY2 = (float) Math.toRadians(-45);
            float angleZ2 = (float) Math.toRadians(-45);
           
            
            world.addInstance(Compounds.newRoundBox(true, 0.05f, new Phong(0.2f, 0.65f, 0.4f, 64.0f, new Vector3(0.0f, 0.0f, 1.0f)))) //azul
                    .scale(60)
                    .translate(0, -50, 0);
            world.addInstance(Compounds.newRoundBox(true, 0.05f, new Phong(0.2f, 0.65f, 0.4f, 64.0f, new Vector3(0.0f, 1.0f, 0.0f)))) //verde
            		.scale(60)
            		.translate(120, -50, 0);
            world.addInstance(Compounds.newRoundBox(true, 0.05f, new Phong(0.2f, 0.65f, 0.4f, 64.0f, new Vector3(1.0f, 0.0f, 0.0f)))) //vermelho
            		.scale(60)
            		.translate(80, -50, -60);
            world.addInstance(Compounds.newRoundBox(true, 0.05f, new Phong(0.2f, 0.65f, 0.4f, 64.0f, new Vector3(0.2f, 1.0f, 0.8f)))) //cyano
    				.scale(60)
    				.rotateX(angleX2)
    				.rotateY(angleY2)
    				.rotateZ(angleZ2)
    				.translate(150, 20, -30);
            world.addInstance(Compounds.newRoundBox(true, 0.05f, new Phong(0.2f, 0.65f, 0.4f, 64.0f, new Vector3(1.0f, 0.6f, 0.0f)))) //laranja
    				.scale(60)
    				.rotateX(angleX1)
    				.rotateY(angleY1)
    				.translate(40, 10, -40);
            world.addInstance(Compounds.newRoundBox(true, 0.05f, new Phong(0.2f, 0.65f, 0.4f, 64.0f, new Vector3(1.0f, 0.0f, 1.0f)))) //Rosa
    				.scale(60)
            		.rotateX(angleX)
            		.rotateY(angleY)
            		.translate(-65, -50, 50);
                        

            world.add(new Plane(new Vector3(0, -80, 0), new Vector3(0, 1, 0), new Fosco(0.2f, 0.5f, new Vector3(1, 1, 1))));
            return world;
        }

	
    };
    

    public abstract World createScene(int numSamples, float zoom, WorldListener listener);

    @Override
    public String toString() {
        String name = super.toString();
        return name.charAt(0) + name.substring(1).toLowerCase();
    }

}
