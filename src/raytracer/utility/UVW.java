package raytracer.utility;

import raytracer.math.Vector2;
import raytracer.math.Vector3;

import static raytracer.math.Vector3.multiply;

public class UVW {
    private Vector3 u;
    private Vector3 v;
    private Vector3 w;

    public UVW(Vector3 u, Vector3 v, Vector3 w) {
        this.u = u;
        this.v = v;
        this.w = w;
    }

    public Vector3 getU() {
        return u;
    }

    public Vector3 getV() {
        return v;
    }

    public Vector3 getW() {
        return w;
    }

    public Vector3 transform(Vector3 vec) {
        return multiply(u, vec.getX())
                .add(multiply(v, vec.getY()))
                .add(multiply(w, vec.getZ()));
    }

    public Vector3 transform(Vector2 vec, float z) {
        return multiply(u, vec.getX())
                .add(multiply(v, vec.getY()))
                .add(multiply(w, z));
    }

}