package raytracer.utility;

/**
 * A class to allow passing a single floating point value by reference.
 */
public class FloatRef {
    public float value;
}
