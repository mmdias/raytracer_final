package raytracer.utility;

public class RaytraceException extends IllegalArgumentException {

	private static final long serialVersionUID = 1L;

	public RaytraceException(String message, Object... params) {
        super(String.format(message, params));
    }

    public RaytraceException(Throwable cause, String message, Object... params) {
        super(String.format(message, params), cause);
    }

    public RaytraceException(Throwable cause) {
        this(cause, "A problem occured while ray tracing!");
    }
}
