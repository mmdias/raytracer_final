package raytracer.utility;

import java.util.Random;

/**
 * Random number generator utilities
 */
public class Rnd {
    private static final Random RND = new Random();

    public synchronized static float rndFloat() {
        return RND.nextFloat();
    }

    public static float rndFloat(float min, float max) {
        return min + rndFloat() * (max - min);
    }

    public synchronized static int rndInt(int range) {
        return RND.nextInt(range);
    }

    public synchronized static int rndInt(int min, int max) {
        int range = max - min;
        if (range == 0)
            return min;

        return min + RND.nextInt(range);
    }
}
