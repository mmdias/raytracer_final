package raytracer.material;

import raytracer.brdf.Difuse;
import raytracer.light.Light;
import raytracer.math.Vector3;
import raytracer.utility.ShadeRec;

import static raytracer.math.Vector3.mul;

public class Fosco extends AbstractMaterial {

    private Difuse difuse;

    public Fosco(float ka, float kd, Vector3 color) {
        super(ka, color);
        difuse = new Difuse(kd, color);
    }


    public void setKd(float k) {
        difuse.setKd(k);
    }

    public float getKd() {
        return difuse.getKd();
    }

    @Override
    public void setCd(Vector3 color) {
        ambient.setCd(color);
        difuse.setCd(color);
    }

    @Override
    protected Vector3 processLight(ShadeRec sr, Vector3 wo, Light light, Vector3 wi, float ndotwi) {
        return mul(difuse.f(sr, wo, wi), light.L(sr)).multiply(ndotwi);
    }

    @Override
    protected Vector3 processAreaLight(ShadeRec sr, Vector3 wo, Light light, Vector3 wi, float ndotwi) {
        return processLight(sr, wo, light, wi, ndotwi).multiply(light.G(sr) / light.pdf(sr));
    }

    @Override
    public Fosco clone() {
        return new Fosco(ambient.getKd(), difuse.getKd(), ambient.getCd().clone());
    }
}
