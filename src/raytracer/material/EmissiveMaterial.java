package raytracer.material;

import raytracer.math.Vector3;
import raytracer.utility.ShadeRec;

public interface EmissiveMaterial extends Material {
    /**
     * Returns the light brightness at the given point
     *
     * @param sr The shade rect of the point
     * @return the light brightness at the given point
     */
    Vector3 getLe(ShadeRec sr);

    /**
     * @return Clones this material
     */
    @Override
	EmissiveMaterial clone();
}
