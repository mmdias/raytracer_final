package raytracer.material;

import raytracer.math.Vector3;
import raytracer.utility.ShadeRec;

import static raytracer.math.Vector3.multiply;
import static raytracer.math.Vector3.negate;

/**
 * Represents a material that can emit light.
 */
public class Emissivo implements EmissiveMaterial {
    private float ls;
    private Vector3 ce;

    public Emissivo(float ls, Vector3 color) {
        this.ls = ls;
        this.ce = color;
    }

    @Override
	public Vector3 getLe(ShadeRec sr) {
        return multiply(ce, ls);
    }

    @Override
    public Vector3 shade(ShadeRec sr) {
        return negate(sr.normal).dot(sr.ray.getDirection()) > 0 ?
                multiply(ce, ls) : new Vector3();
    }

    @Override
    public Vector3 areaLightShade(ShadeRec sr) {
        return shade(sr);
    }

    @Override
    public Emissivo clone() {
        return new Emissivo(ls, ce.clone());
    }
}
