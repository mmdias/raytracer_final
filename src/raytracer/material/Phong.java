package raytracer.material;

import raytracer.brdf.GlossySpecular;
import raytracer.brdf.Difuse;
import raytracer.light.Light;
import raytracer.math.Vector3;
import raytracer.utility.ShadeRec;

import static raytracer.math.Vector3.mul;

public class Phong extends AbstractMaterial {
    private Difuse diffuse;
    private GlossySpecular specular;

    public Phong(float ka, float kd, float ks, float exp, Vector3 color) {
        super(ka, color);
        diffuse = new Difuse(kd, color);
        specular = new GlossySpecular(ks, color, exp);
    }

    public void setKd(float k) {
        diffuse.setKd(k);
    }

    public float getKd() {
        return diffuse.getKd();
    }

    @Override
    public void setCd(Vector3 color) {
        ambient.setCd(color);
        diffuse.setCd(color);
    }

    public void setKs(float k) {
        specular.setKs(k);
    }

    public float getKs() {
        return specular.getKs();
    }

    public void setExp(float exp) {
        specular.setExp(exp);
    }

    public float getExp() {
        return specular.getExp();
    }


    public void setCs(Vector3 color) {
        specular.setCs(color);
    }

    public Vector3 getCs() {
        return specular.getCs();
    }

    @Override
	protected Vector3 processLight(ShadeRec sr, Vector3 wo, Light light, Vector3 wi, float ndotwi) {
        return mul(diffuse.f(sr, wo, wi).add(specular.f(sr, wo, wi)), light.L(sr)).multiply(ndotwi);
    }

    @Override
    public Phong clone() {
        Phong p = new Phong(getKa(), getKd(), getKs(), getExp(), getCd().clone());
        p.setCs(getCs().clone());
        return p;
    }
}