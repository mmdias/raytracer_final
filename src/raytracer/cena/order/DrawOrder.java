package raytracer.cena.order;

public interface DrawOrder {
    public PixelArray getPixels(int w, int h);
}