package raytracer.cena.order;

public enum DrawOrders implements DrawOrder {
    NORMAL {
        @Override
        public PixelArray getPixels(int w, int h) {
            return new PixelArray(w, h).setNaturalOrder();
        }
    },
    RANDOM {
        @Override
  public PixelArray getPixels(int w, int h) {
            PixelArray pixels = NORMAL.getPixels(w, h);
            pixels.shuffle();
            return pixels;
        }
    };

    @Override
    public String toString() {
        String name = super.toString();
        return name.charAt(0) + name.substring(1).toLowerCase();
    }
}
