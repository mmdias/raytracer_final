package raytracer.cena;

import raytracer.math.Vector3;

public interface WorldListener {

    /**
     * Indicate the scene rendering begun.
     *
     * @param world  World, source of the event
     * @param width  Viewport width
     * @param height Viewport height
     */
    void traceStarted(World world, int width, int height);

    /**
     * Indicate that a pixel was traced.
     *
     * @param world World, source of the event
     * @param x     Pixel x position
     * @param y     Pixel y position
     * @param color Pixel unsaturated pixel color. Use the Vector3#toRGB() method to
     *              saturate and convert to a RGB format.
     */
    void pixelTraced(World world, int x, int y, Vector3 color);

    /**
     * Indicate that the image was fully rendered.
     *
     * @param world      World, source of the event
     * @param renderTime Time of rendering, in milliseconds.
     */
    void traceFinished(World world, long renderTime);
}
