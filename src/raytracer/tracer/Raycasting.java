package raytracer.tracer;

import raytracer.cena.World;
import raytracer.math.Ray;
import raytracer.math.Vector3;
import raytracer.utility.ShadeRec;

/**
 * Simple ray casting algorithm
 */
public class Raycasting implements Tracer {

    @Override
    public Vector3 trace(World world, Ray ray, int depth) {
        ShadeRec sr = world.hit(ray).clone();
        if (sr.hitAnObject) {
            sr.ray = ray;
            return sr.material.shade(sr);
        }
        return world.getBackgroundColor();
    }
}
