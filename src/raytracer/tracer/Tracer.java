package raytracer.tracer;

import raytracer.cena.World;
import raytracer.math.Ray;
import raytracer.math.Vector3;

/**
 * Represent a tracing strategy
 */
public interface Tracer {
    Vector3 trace(World world, Ray ray, int depth);
}