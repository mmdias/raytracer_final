package raytracer.sampler;

import raytracer.math.Vector2;

import java.util.List;

/**
 * A sample is a set of distributed points in a (0,1)-(0,1) space.
 * There are several algorithms to generate samples, usually in a random pattern.
 */
public interface Sample {
    /**
     * Create a set of samples.
     *
     * @param numSamples Number of samples in the set
     * @return The samples.
     * @throws raytracer.utility.RaytraceException
     *          If tne number of samples is smaller than 1.
     */
    List<Vector2> createSamples(int numSamples);
}
