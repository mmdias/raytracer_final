package raytracer.math;

public class Ray {
    private Vector3 origin;
    private Vector3 direction;

    public Ray(Vector3 origin, Vector3 direction) {
        this.direction = Vector3.normalize(direction);
        this.origin = origin;
    }

    public Vector3 getOrigin() {
        return origin;
    }

    public void setOrigin(Vector3 origin) {
        this.origin = origin.clone();
    }

    public Vector3 getDirection() {
        return direction;
    }

    public void setDirection(Vector3 direction) {
        this.direction = Vector3.normalize(direction);
    }

    public Vector3 pointAt(float t) {
        return Vector3.multiply(direction, t).add(origin);
    }
}