package raytracer.math.geometricas;

import raytracer.material.EmissiveMaterial;
import raytracer.math.Vector3;
import raytracer.utility.ShadeRec;

public interface EmissiveObject {
   
    Vector3 sample();

    float pdf(ShadeRec sr);

    Vector3 getNormal(Vector3 p);

    EmissiveObject clone();

    EmissiveMaterial getEmissiveMaterial();
}

