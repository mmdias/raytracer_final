package raytracer.math.geometricas.primitivas;

import raytracer.material.Material;
import raytracer.math.Ray;
import raytracer.math.Vector3;
import raytracer.math.geometricas.GeometricObject;
import raytracer.utility.FloatRef;
import raytracer.utility.ShadeRec;
import static raytracer.math.Vector3.normalize;
import static raytracer.math.Vector3.subtract;

public class Disco implements GeometricObject {
    private Vector3 center;
    private Vector3 normal;
    private float radius;

    private Material material;

    /**
     * Creates a new generic disk centered at origin, pointing up and with radius 1.
     *
     * @param material The disk material
     */
    public Disco(Material material) {
        this(new Vector3(), new Vector3(0, 1, 0), 1.0f, material);
    }

    public Disco(Vector3 center, Vector3 normal, float radius, Material material) {
        this.center = center;
        this.normal = normalize(normal);
        this.radius = radius;
        this.material = material;
    }

    @Override
    public boolean hit(Ray ray, ShadeRec sr, FloatRef tmin) {
        float t = subtract(center, ray.getOrigin()).dot(normal) / ray.getDirection().dot(normal);
        if (t < K_EPSILON) {
            return false;
        }

        Vector3 p = ray.pointAt(t);
        if (center.distanceSqr(p) >= radius * radius) {
            return false;
        }

        tmin.value = t;
        sr.normal = normal;
        sr.worldHitPoint = p;
        sr.localHitPoint = sr.worldHitPoint;
        return true;
    }

    @Override
    public boolean shadow_hit(Ray ray, FloatRef tmin) {
        float t = subtract(center, ray.getOrigin()).dot(normal) / ray.getDirection().dot(normal);
        if (t < K_EPSILON) {
            return false;
        }

        tmin.value = t;
        Vector3 p = ray.pointAt(t);
        return (center.distanceSqr(p) < radius * radius);
    }

    @Override
    public Material getMaterial() {
        return material;
    }
}
