package raytracer.math.geometricas.primitivas;

import raytracer.material.Material;
import raytracer.math.Ray;
import raytracer.math.Vector3;
import raytracer.math.geometricas.GeometricObject;
import raytracer.utility.FloatRef;
import raytracer.utility.ShadeRec;
import static raytracer.math.Vector3.normalize;
import static raytracer.math.Vector3.subtract;

public class Anel implements GeometricObject {
    private Vector3 center;
    private Vector3 normal;
    private float outerRadius;
    private float innerRadius;

    private Material material;


    public Anel(Material material) {
        this(new Vector3(), new Vector3(0, 1, 0), 0.5f, 1.0f, material);
    }

    public Anel(Vector3 center, Vector3 normal, float innerRadius, float outerRadius, Material material) {
        this.center = center;
        this.normal = normalize(normal);
        this.outerRadius = outerRadius;
        this.innerRadius = innerRadius;
        this.material = material;
    }

    @Override
    public boolean hit(Ray ray, ShadeRec sr, FloatRef tmin) {
        float t = subtract(center, ray.getOrigin()).dot(normal) / ray.getDirection().dot(normal);
        if (t < K_EPSILON) {
            return false;
        }

        Vector3 p = ray.pointAt(t);
        float centerDistanceSqr = center.distanceSqr(p);

        if ((centerDistanceSqr >= outerRadius * outerRadius) || (centerDistanceSqr <= innerRadius * innerRadius)) {
            return false;
        }

        tmin.value = t;
        sr.normal = normal;
        sr.worldHitPoint = p;
        sr.localHitPoint = sr.worldHitPoint;
        return true;
    }

    @Override
    public boolean shadow_hit(Ray ray, FloatRef tmin) {
        float t = subtract(center, ray.getOrigin()).dot(normal) / ray.getDirection().dot(normal);
        if (t < K_EPSILON) {
            return false;
        }

        tmin.value = t;
        Vector3 p = ray.pointAt(t);
        float centerDistanceSqr = center.distanceSqr(p);
        return (centerDistanceSqr < outerRadius * outerRadius) && (centerDistanceSqr >= innerRadius * innerRadius);

    }

    @Override
    public Material getMaterial() {
        return material;
    }
}
