package raytracer.math.geometricas.primitivas;


import raytracer.material.EmissiveMaterial;
import raytracer.material.Material;
import raytracer.math.Ray;
import raytracer.math.Vector2;
import raytracer.math.Vector3;
import raytracer.math.geometricas.EmissiveObject;
import raytracer.math.geometricas.GeometricObject;
import raytracer.sampler.Sampler;
import raytracer.utility.FloatRef;
import raytracer.utility.ShadeRec;
import static raytracer.math.Vector3.*;

public class Retangulo implements GeometricObject, EmissiveObject {

    private Vector3 p0;            // corner vertex
    private Vector3 a;              // side
    private double aLenSquared;
    private Vector3 b;                // side
    private double bLenSquared;

    private Vector3 normal;

    private float invArea;
    private Sampler sampler;

    private Material material;


    public Retangulo(Vector3 p0, Vector3 a, Vector3 b, Vector3 normal, Material material) {
        this.p0 = p0;

        this.a = a;
        this.aLenSquared = a.sizeSqr();

        this.b = b;
        this.bLenSquared = b.sizeSqr();

        this.invArea = 1.0f / a.size() * b.size();

        this.normal = normalize(normal);
        this.material = material;
    }

    public Retangulo(Vector3 p0, Vector3 a, Vector3 b, Material material) {
        this(p0, a, b, cross(a, b).normalize(), material);
    }

    public Retangulo(Material material) {
        this.p0 = new Vector3(-1, 0, -1);

        this.a = new Vector3(0, 0, 1);
        this.aLenSquared = 1;

        this.b = new Vector3(1, 0, 0);
        this.bLenSquared = 1;

        this.invArea = 0.5f;

        this.normal = new Vector3(0, 1, 0);
        this.material = material;
    }

    public Retangulo(Vector3 p0, Vector3 a, Vector3 b, Vector3 normal, EmissiveMaterial material, Sampler sampler) {
        this(p0, a, b, normal, material);
        this.sampler = sampler;
    }

    public Retangulo(Vector3 p0, Vector3 a, Vector3 b, EmissiveMaterial material, Sampler sampler) {
        this(p0, a, b, material);
        this.sampler = sampler;
    }

    public Retangulo(EmissiveMaterial material, Sampler sampler) {
        this(material);
        this.sampler = sampler;
    }

    public Vector3 getP0() {
        return p0;
    }

    public Vector3 getA() {
        return a;
    }

    public Vector3 getB() {
        return b;
    }

    @Override
    public Vector3 sample() {
        Vector2 sp = sampler.nextSampleSquare();
        return multiply(a, sp.getX()).add(multiply(b, sp.getY())).add(p0);
    }

    @Override
    public float pdf(ShadeRec sr) {
        return invArea;
    }

    @Override
    public Vector3 getNormal(Vector3 p) {
        return normal;
    }

    @Override
    public Retangulo clone() {
        Retangulo rect = new Retangulo(p0.clone(), a.clone(), b.clone(), normal.clone(), material.clone());
        rect.sampler = sampler == null ? null : sampler.clone();
        return rect;
    }

    @Override
    public boolean hit(Ray ray, ShadeRec sr, FloatRef tmin) {
        float t = subtract(p0, ray.getOrigin()).dot(normal) / ray.getDirection().dot(normal);

        if (t <= K_EPSILON)
            return false;

        Vector3 p = ray.pointAt(t);
        Vector3 d = subtract(p, p0);

        double ddota = d.dot(a);
        if (ddota < 0.0 || ddota > aLenSquared)
            return false;

        double ddotb = d.dot(b);
        if (ddotb < 0.0 || ddotb > bLenSquared)
            return false;

        tmin.value = t;
        sr.normal = normal;
        sr.worldHitPoint = p;
        sr.localHitPoint = sr.worldHitPoint;
        return true;
    }

    @Override
    public boolean shadow_hit(Ray ray, FloatRef tmin) {
        if (material instanceof EmissiveMaterial)
            return false;

        float t = subtract(p0, ray.getOrigin()).dot(normal) / ray.getDirection().dot(normal);

        if (t <= K_EPSILON)
            return false;

        Vector3 p = ray.pointAt(t);
        Vector3 d = subtract(p, p0);

        double ddota = d.dot(a);
        if (ddota < 0.0 || ddota > aLenSquared)
            return false;

        double ddotb = d.dot(b);
        if (ddotb < 0.0 || ddotb > bLenSquared)
            return false;

        tmin.value = t;
        return true;
    }

    @Override
    public Material getMaterial() {
        return material;
    }

    @Override
    public EmissiveMaterial getEmissiveMaterial() {
        return (EmissiveMaterial) material;
    }
}
