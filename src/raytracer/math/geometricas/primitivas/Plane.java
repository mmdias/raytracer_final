package raytracer.math.geometricas.primitivas;

import raytracer.material.Material;
import raytracer.math.Ray;
import raytracer.math.Vector3;
import raytracer.math.geometricas.GeometricObject;
import raytracer.utility.FloatRef;
import raytracer.utility.ShadeRec;
import static raytracer.math.Vector3.normalize;
import static raytracer.math.Vector3.subtract;

public class Plane implements GeometricObject {
    private Vector3 point;
    private Vector3 normal;
    private Material material;

    public Plane(Vector3 point, Vector3 normal, Material material) {
        this.point = point;
        this.normal = normalize(normal);
        this.material = material;
    }

    public Vector3 getPoint() {
        return point;
    }

    public Vector3 getNormal() {
        return normal;
    }

    @Override
    public boolean hit(Ray ray, ShadeRec sr, FloatRef tmin) {
        if (shadow_hit(ray, tmin)) {
            sr.normal = normal;
            sr.worldHitPoint = ray.pointAt(tmin.value);
            sr.localHitPoint = sr.worldHitPoint;
            return true;
        }
        return false;
    }

    @Override
    public boolean shadow_hit(Ray ray, FloatRef tmin) {
        float t = subtract(point, ray.getOrigin()).dot(normal) / ray.getDirection().dot(normal);
        if (t < K_EPSILON)
            return false;

        tmin.value = t;
        return true;
    }

    @Override
    public Material getMaterial() {
        return material;
    }
}
