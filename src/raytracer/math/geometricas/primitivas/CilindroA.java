

package raytracer.math.geometricas.primitivas;


import raytracer.material.Material;
import raytracer.math.BBox;
import raytracer.math.Ray;
import raytracer.math.Vector3;
import raytracer.math.geometricas.GeometricObject;
import raytracer.utility.FloatRef;
import raytracer.utility.ShadeRec;
import static java.lang.Math.sqrt;

public class CilindroA implements GeometricObject {

    private float y0;
    private float y1;
    private float radius;
    private Material material;

    public CilindroA(Material material) {
        this(-0.5f, 0.5f, 1.0f, material);
    }

    public CilindroA(float y0, float y1, float radius, Material material) {
        this.y0 = y0;
        this.y1 = y1;
        this.radius = radius;
        this.material = material;
    }

    @Override
    public boolean hit(Ray ray, ShadeRec sr, FloatRef tmin) {
        if (!shadow_hit(ray, tmin))
            return false;

        float ox = ray.getOrigin().getX();
        float oz = ray.getOrigin().getZ();
        float dx = ray.getDirection().getX();
        float dz = ray.getDirection().getZ();
        float t = tmin.value;
        float inv_radius = 1.0f / radius;

        sr.normal = new Vector3((ox + t * dx) * inv_radius, 0.0f, (oz + t * dz) * inv_radius);
        if (Vector3.negate(ray.getDirection()).dot(sr.normal) < 0.0)
            sr.normal.negate();
        sr.worldHitPoint = ray.pointAt(tmin.value);
        sr.localHitPoint = sr.worldHitPoint;
        return true;
    }

    @Override
    public boolean shadow_hit(Ray ray, FloatRef tmin) {
        float ox = ray.getOrigin().getX();
        float oy = ray.getOrigin().getY();
        float oz = ray.getOrigin().getZ();
        float dx = ray.getDirection().getX();
        float dy = ray.getDirection().getY();
        float dz = ray.getDirection().getZ();

        float a = dx * dx + dz * dz;
        float b = 2.0f * (ox * dx + oz * dz);
        float c = ox * ox + oz * oz - radius * radius;
        float disc = b * b - 4.0f * a * c;

        if (disc < 0.0)
            return false;

        float e = (float) sqrt(disc);
        float denom = 2.0f * a;
        float t = (-b - e) / denom;    // smaller root

        if (t > K_EPSILON) {
            double yhit = oy + t * dy;

            if (yhit > y0 && yhit < y1) {
                tmin.value = t;
                return true;
            }
        }

        t = (-b + e) / denom;    // bigger root
        if (t > K_EPSILON) {
            double yhit = oy + t * dy;
            if (yhit > y0 && yhit < y1) {
                tmin.value = t;
                return true;
            }
        }

        return false;
    }

    public BBox getBounds() {
        return new BBox(-radius, radius, y0, y1, -radius, radius);
    }

    @Override
    public Material getMaterial() {
        return material;
    }
}
