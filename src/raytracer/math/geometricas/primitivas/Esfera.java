

package raytracer.math.geometricas.primitivas;


import raytracer.material.Material;
import raytracer.material.Phong;
import raytracer.math.BBox;
import raytracer.math.Ray;
import raytracer.math.Vector3;
import raytracer.math.geometricas.GeometricObject;
import raytracer.utility.FloatRef;
import raytracer.utility.ShadeRec;
import static raytracer.math.Vector3.multiply;
import static raytracer.math.Vector3.subtract;

public class Esfera implements GeometricObject {
    private Vector3 center;
    private float radius;
    private Material material;

    /**
     * Creates a generic sphere centered in the origin and with radius 1.
     *
     * @param material The sphere material.
     */
    public Esfera(Material material) {
        this(new Vector3(), 1.0f, material);
    }

    /**
     * Creates a sphere with the given center, radius and material.
     *
     * @param center   Esfera center
     * @param radius   Esfera radius
     * @param material Esfera material
     */
    public Esfera(Vector3 center, float radius, Material material) {
        this.center = center;
        this.radius = radius;
        this.material = material;
    }

    /**
     * Creates a new sphere with the given center, radius and a default material of the given color.
     *
     * @param center The sphere center
     * @param radius The sphere radius
     * @param color  The sphere color
     */
    public Esfera(Vector3 center, float radius, Vector3 color) {
        this(center, radius, new Phong(0.2f, 0.65f, 0.1f, 8.00f, color));
    }


    public Vector3 getCenter() {
        return center;
    }

    public float getRadius() {
        return radius;
    }

    @Override
    public boolean hit(Ray ray, ShadeRec sr, FloatRef tmin) {
        if (shadow_hit(ray, tmin)) {
            Vector3 temp = subtract(ray.getOrigin(), center);
            sr.normal = multiply(ray.getDirection(), tmin.value).add(temp).divide(radius);
            sr.worldHitPoint = ray.pointAt(tmin.value);
            sr.localHitPoint = sr.worldHitPoint;
            return true;
        }
        return false;
    }

    @Override
    public boolean shadow_hit(Ray ray, FloatRef tmin) {
        Vector3 temp = subtract(ray.getOrigin(), center);

        //Bhaskara equation a, b and c terms and delta calculation (bˆ2 - 4ac).

        //float a = 1, since ray.getDirection().sizeSqr() is always one.
        float b = multiply(temp, 2.0f).dot(ray.getDirection());
        float c = temp.sizeSqr() - radius * radius;
        float delta = b * b - 4.0f * c;

        if (delta < 0.0) {
            return false;
        }

        float e = (float) Math.sqrt(delta);

        //Smaller root
        float t = (-b - e) / 2.0f;
        if (t <= K_EPSILON) {
            //If not hit, tries the larger root
            t = (-b + e) / 2.0f;
        }

        if (t > K_EPSILON) {
            tmin.value = t;
            return true;
        }

        //Too close to hit
        return false;
    }

    @Override
    public Material getMaterial() {
        return material;
    }

    public BBox getBounds() {
        return new BBox(-radius, radius, -radius, radius, -radius, radius);
    }

}
