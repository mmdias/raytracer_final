package raytracer.math.geometricas.compound;

import raytracer.material.Material;
import raytracer.math.BBox;
import raytracer.math.Ray;
import raytracer.math.Vector3;
import raytracer.math.geometricas.GeometricObject;
import raytracer.math.geometricas.Instance;
import raytracer.utility.FloatRef;
import raytracer.utility.ShadeRec;

import java.util.ArrayList;
import java.util.List;

public class Compound implements GeometricObject {
    private List<GeometricObject> objects = new ArrayList<GeometricObject>();
    private BBox bounds;

    public Compound add(GeometricObject obj) {
        objects.add(obj);
        return this;
    }

    public Instance addInstance(GeometricObject obj) {
        Instance instance = new Instance(obj);
        objects.add(instance);
        return instance;
    }

    public Instance addInstance(GeometricObject obj, Material mtrl) {
        Instance instance = new Instance(obj, mtrl);
        objects.add(instance);
        return instance;
    }

    @Override
    public boolean hit(Ray ray, ShadeRec sr, FloatRef tmin) {
        if (bounds != null && !bounds.hit(ray))
            return false;

        Vector3 normal = null;
        Vector3 worldHitPoint = null;
        Vector3 localHitPoint = null;
        float tMin = Float.MAX_VALUE;
        boolean hit = false;

        for (GeometricObject obj : objects) {
            FloatRef fr = new FloatRef();
            if (obj.hit(ray, sr, fr) && fr.value < tMin) {
                hit = true;
                tMin = fr.value;
                //sr.material = obj.getMaterial();
                normal = sr.normal;
                worldHitPoint = sr.worldHitPoint;
                localHitPoint = sr.localHitPoint;
            }
        }

        if (hit) {
            tmin.value = tMin;
            sr.normal = normal;
            sr.worldHitPoint = worldHitPoint;
            sr.localHitPoint = localHitPoint;
        }
        return hit;
    }

    @Override
    public boolean shadow_hit(Ray ray, FloatRef tmin) {
        if (bounds != null && !bounds.hit(ray))
            return false;

        float tMin = Float.MAX_VALUE;
        boolean hit = false;

        for (GeometricObject obj : objects) {
            FloatRef fr = new FloatRef();
            if (obj.shadow_hit(ray, fr) && fr.value < tMin) {
                hit = true;
                tMin = fr.value;
            }
        }

        if (hit) {
            tmin.value = tMin;
        }
        return hit;
    }

    @Override
    public Material getMaterial() {
        return objects.get(0).getMaterial();
    }

    public void setBounds(BBox bounds) {
        this.bounds = bounds;
    }

    public BBox getBounds() {
        return bounds;
    }

}
