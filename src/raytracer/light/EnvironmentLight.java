package raytracer.light;

import raytracer.material.EmissiveMaterial;
import raytracer.math.Ray;
import raytracer.math.Vector3;
import raytracer.sampler.Sampler;
import raytracer.utility.ShadeRec;
import raytracer.utility.UVW;

import static raytracer.math.Vector3.cross;

public class EnvironmentLight extends AbstractLight {
    private Sampler sampler;
    private EmissiveMaterial material;
    private UVW uvw;
    public EnvironmentLight(EmissiveMaterial material, Sampler sampler) {
        this.sampler = sampler;
        this.material = material;
    }

    @Override
    public Vector3 getDirection(ShadeRec sr) {
        Vector3 w = sr.normal;
        Vector3 v = cross(new Vector3(0.0034f, 1.0000f, 0.0071f), w).normalize();
        Vector3 u = cross(v, w);

        uvw = new UVW(u, v, w);
        return uvw.transform(sampler.nextSampleHemisphere());
    }

    @Override
    public Vector3 L(ShadeRec sr) {
        return material.getLe(sr);
    }

    @Override
    public boolean inShadow(Ray ray, ShadeRec sr) {
        return sr.world.shadowHit(ray, Float.MAX_VALUE);
    }

    @Override
    public EnvironmentLight clone() {
        return new EnvironmentLight(material.clone(), sampler.clone());

    }
}
