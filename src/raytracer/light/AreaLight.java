package raytracer.light;

import raytracer.math.Ray;
import raytracer.math.Vector3;
import raytracer.math.geometricas.EmissiveObject;
import raytracer.utility.ShadeRec;
import static raytracer.math.Vector3.negate;
import static raytracer.math.Vector3.subtract;


public class AreaLight extends AbstractLight {

    private EmissiveObject object;

    private Vector3 samplePoint;
    private Vector3 lightNormal;
    private Vector3 wi;

    public AreaLight(EmissiveObject object) {
        this.object = object;
    }

    @Override
    public Vector3 getDirection(ShadeRec sr) {
        samplePoint = object.sample();
        lightNormal = object.getNormal(samplePoint);
        wi = subtract(samplePoint, sr.worldHitPoint).normalize();
        return wi;
    }

    @Override
    public Vector3 L(ShadeRec sr) {
        return negate(lightNormal).dot(wi) > 0.0f ?
                object.getEmissiveMaterial().getLe(sr) : new Vector3();
    }

    @Override
    public float G(ShadeRec sr) {
        float ndotd = negate(lightNormal).dot(wi);
        float d2 = samplePoint.distanceSqr(sr.worldHitPoint);
        return ndotd / d2;
    }

    @Override
    public float pdf(ShadeRec sr) {
        return object.pdf(sr);
    }

    @Override
    public boolean inShadow(Ray ray, ShadeRec sr) {
        float ts = subtract(samplePoint, ray.getOrigin()).dot(ray.getDirection());
        return sr.world.shadowHit(ray, ts);
    }

    @Override
    public AreaLight clone() {
        return new AreaLight(object.clone());
    }
}
