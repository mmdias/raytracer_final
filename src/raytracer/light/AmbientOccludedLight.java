package raytracer.light;

import raytracer.math.Ray;
import raytracer.math.Vector3;
import raytracer.sampler.Sampler;
import raytracer.utility.ShadeRec;
import raytracer.utility.UVW;

import static raytracer.math.Vector3.cross;
import static raytracer.math.Vector3.multiply;


public class AmbientOccludedLight extends AbstractLight {
    private float ls;
    private Vector3 color;

    private float minAmount;
    private Sampler sampler;
    private UVW uvw;

    public AmbientOccludedLight(float ls, Vector3 color, float minAmount, Sampler sampler) {
        this.ls = ls;
        this.color = color;
        this.minAmount = minAmount;
        this.sampler = sampler;
    }

    public float getLs() {
        return ls;
    }

    public void setLs(float ls) {
        this.ls = ls;
    }

    public Vector3 getColor() {
        return color;
    }

    public void setColor(Vector3 color) {
        this.color = color;
    }

    @Override
    public Vector3 getDirection(ShadeRec sr) {
        return uvw.transform(sampler.nextSampleHemisphere());
    }

    @Override
    public boolean inShadow(Ray ray, ShadeRec sr) {
        return sr.world.shadowHit(ray, Float.MAX_VALUE);
    }

    @Override
    public Vector3 L(ShadeRec sr) {
        Vector3 w = sr.normal;
        Vector3 v = cross(w, new Vector3(0.0072f, 1.0f, 0.0034f));
        Vector3 u = cross(v, w);
        uvw = new UVW(u, v, w);

        Ray shadow_ray = new Ray(sr.worldHitPoint, getDirection(sr));
        if (inShadow(shadow_ray, sr))
            return multiply(color, ls * minAmount);
        else
            return multiply(color, ls);
    }

    @Override
    public AmbientOccludedLight clone() {
        return new AmbientOccludedLight(ls, color.clone(), minAmount, sampler.clone());
    }
}
