package raytracer.light;


import raytracer.utility.ShadeRec;

public abstract class AbstractLight implements Light {
    private boolean castShadows = true;

    public AbstractLight() {
    }

    @Override
    public float G(ShadeRec sr) {
        return 1.0f;
    }

    @Override
    public float pdf(ShadeRec sr) {
        return 1.0f;
    }

    @Override
    public boolean castShadows() {
        return castShadows;
    }

    public void setCastShadows(boolean castShadows) {
        this.castShadows = castShadows;
    }

    @Override
	public abstract Light clone();
}
