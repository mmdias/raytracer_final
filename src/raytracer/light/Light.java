

package raytracer.light;

import raytracer.math.Ray;
import raytracer.math.Vector3;
import raytracer.utility.ShadeRec;

public interface Light extends Cloneable {

    Vector3 getDirection(ShadeRec sr);
    Vector3 L(ShadeRec sr);
    float G(ShadeRec sr);
    float pdf(ShadeRec sr);
    boolean castShadows();
    boolean inShadow(Ray ray, ShadeRec sr);
    Light clone();
}